import React, { useMemo, createContext, useContext, useState } from "react";
import axios from "axios";
import useSWR from "swr";
import Loader from "react-loader-spinner";
import styled from "styled-components";

export const Loading = styled.div`
  left: 50%;
  top: 50%;
  position: relative;
  margin-left: -40px;
  margin-top: -40px;
`;

export const ProductsContext = createContext([]);
export function ProductsProvider({ children }) {
  const fetcher = (url) => axios.get(url).then((res) => res.data);
  const { data, error } = useSWR(
    "https://run.mocky.io/v3/66063904-d43c-49ed-9329-d69ad44b885e",
    fetcher
  );
  const resp = useMemo(() => data, error);
  if (error) return <div>failed to load</div>;
  if (!data) {
    return (
      <>
        <Loading>
          <Loader
            type="Puff"
            color="#00BFFF"
            height={100}
            width={100}
            timeout={3000}
          />
        </Loading>
      </>
    );
  }

  return (
    <ProductsContext.Provider value={resp}>{children}</ProductsContext.Provider>
  );
}
export function useProducts() {
  const context = useContext(ProductsContext);
  if (!context) {
    throw new Error("Context must be used within a Provider");
  }
  return context;
}
