let productsId = JSON.parse(localStorage.getItem("products_id") || "[]");

export function ControllerLocalStorage(isLiked, id) {
  console.log(isLiked);
  if (!isLiked) {
    console.log("entrei 1");
    try {
      if (!localStorage.hasOwnProperty("products_id")) {
        productsId.push(id);
        localStorage.setItem("products_id", JSON.stringify(productsId));
      } else {
        if (productsId.indexOf(id) === -1) {
          productsId.push(id);
          localStorage.setItem("products_id", JSON.stringify(productsId));
        }
      }
    } catch (error) {
      console.error(error);
    }
  } else {
    console.log("entrei");
    let index = productsId.indexOf(id);
    productsId.splice(index, 1);
    localStorage.setItem("products_id", JSON.stringify(productsId));
  }
}

export function GetLocalStorage() {
  let arrayProducts = JSON.parse(localStorage.getItem("products_id") || "[]");
  return arrayProducts;
}

export function RemoveLocalStorage(id) {
  let index = productsId.indexOf(id);
  productsId.splice(index, 1);
  localStorage.setItem("products_id", JSON.stringify(productsId));
  location.reload();
}
