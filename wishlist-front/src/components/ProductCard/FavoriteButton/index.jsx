import React, { useState } from "react";
import Lottie from "react-lottie";
import { ButtonWrapper } from "./styles.js";
import animationData from "./animation.json";
import { ControllerLocalStorage } from "../../util";

const defaultOptions = {
  loop: false,
  autoplay: false,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export const FavoriteButton = (props) => {
  const [isLiked, setLikeState] = useState(false);
  const [animationState, setAnimationState] = useState({
    isStopped: true,
    isPaused: false,
    direction: -1,
  });
  return (
    <>
      <ButtonWrapper
        onClick={() => {
          const reverseAnimation = -1;
          const normalAnimation = 1;

          setAnimationState({
            ...animationState,
            isStopped: false,
            direction:
              animationState.direction === normalAnimation
                ? reverseAnimation
                : normalAnimation,
          });
          setLikeState(!isLiked);
          ControllerLocalStorage(isLiked, props.idProduct);
        }}
      >
        <div className="animation">
          <Lottie
            options={defaultOptions}
            width={200}
            height={200}
            direction={animationState.direction}
            isStopped={animationState.isStopped}
            isPaused={animationState.isPaused}
            speed={1.5}
          />
        </div>
      </ButtonWrapper>
    </>
  );
};
