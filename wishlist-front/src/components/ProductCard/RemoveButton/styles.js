import styled from "styled-components";

export const ButtonWrapper = styled.button`
  --animation-size: 50px;
  z-index: 10;
  float: right;
  width: var(--animation-size);
  height: var(--animation-size);
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  font-size: 33px;
  background-color: transparent;
  border: 0;
  padding: 0;
  cursor: pointer;
  outline: 0;
  border-radius: 100%;
  .button-style {
    pointer-events: none;
  }
`;

export const SpanIcon = styled.span`
  z-index: 10;
`;
