import { ButtonWrapper, SpanIcon } from "./styles.js";
import { RemoveLocalStorage } from "../../util.js";

export const RemoveButton = (props) => {
  return (
    <>
      <ButtonWrapper
        onClick={() => {
          RemoveLocalStorage(props.idProduct);
        }}
      >
        <div className="button-styles">
          <SpanIcon aria-hidden="true">&times;</SpanIcon>
        </div>
      </ButtonWrapper>
    </>
  );
};
