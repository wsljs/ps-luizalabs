import { Card, Image, Title, Price, FavCard } from "./style";
import { FavoriteButton } from "./FavoriteButton";
import { RemoveButton } from "./RemoveButton";

export default function ProductCard(props) {
  const isWishList = props.isWishlist;
  return (
    <>
      <Card>
        {isWishList == false ? (
          <FavCard>{<FavoriteButton idProduct={props.id} />}</FavCard>
        ) : (
          <RemoveButton idProduct={props.id} />
        )}
        <Image src={`${props.src}`} />
        <Title>{props.title}</Title>
        <Price>
          {props.currencyFormat} : {props.price}
        </Price>
      </Card>
    </>
  );
}
