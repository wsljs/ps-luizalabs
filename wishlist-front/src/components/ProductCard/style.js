import styled from "styled-components";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faHeart } from "@fortawesome/free-solid-svg-icons";

export const Card = styled.div`
  margin-left: 10px;
  margin-bottom: 30px;
  width: 300px;
  height: 460px;
  background-color: whitesmoke;
  border: 1px solid #e0e0e0;
  -webkit-box-shadow: 0px 0px 14px -6px rgba(0, 0, 0, 0.3);
  box-shadow: 0px 0px 14px -6px rgba(0, 0, 0, 0.3);
`;

export const Image = styled.img`
  z-index: 1;
  width: 300px;
  height: 300px;
  position: relative;
  top: -50px;
`;

export const Title = styled.p`
  font-weight: 800;
  text-align: center;
`;

export const Price = styled.p`
  font-size: 2rem;
  color: #e5e619;
  font-weight: 900;
  text-align: center;
  margin: 30px;
  /* margin-top: 30; */
`;

export const FavCard = styled.div`
  z-index: 10;

  margin-left: 5px; /* As 4 bordas sólidas com 25px de espessura */
  position: relative;
`;
