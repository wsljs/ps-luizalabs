import React from "react";
import { Route, BrowserRouter } from "react-router-dom";

import Products from "../Products";
import WishList from "../WishList";

const Routes = () => {
  return (
    <BrowserRouter>
      <Route component={Products} path="/" exact />
      <Route component={WishList} path="/wishlist" />
    </BrowserRouter>
  );
};

export default Routes;
