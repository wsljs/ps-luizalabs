import styled from "styled-components";

export const Container = styled.header`
  background-color: var(--first-color);
  width: 100%;
`;

export const Title = styled.h1`
  font-size: 3rem;
  font-weight: 900;
  padding: 10px;
  color: white;
  position: relative;
  text-decoration: none;

  @media (max-width: 1564px) {
    opacity: 0;
    height: auto;
  }
`;

export const Content = styled.div`
  max-width: 1120px;
  margin: 0 auto;
  padding: 0 1rem 0.3rem;
  position: relative;
  top: -50px;

  .flex-container {
    padding: 0;
    margin: 0;
    list-style: none;
    display: flex;
    margin-bottom: 5px;
  }
  .space-between {
    justify-content: space-between;
  }
  .flex-item {
    font-size: 1.3rem;
    color: white;
  }
  a {
    font-size: 1.3rem;
    color: white;
    text-decoration: none;
  }
`;

export const Input = styled.input`
  width: 100%;
  height: 60px;
  position: relative;
`;
