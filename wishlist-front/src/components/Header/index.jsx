import { Container, Content, Input, Title } from "./style";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMapMarkerAlt,
  faPhoneAlt,
  faHeart,
} from "@fortawesome/free-solid-svg-icons";

export default function Header() {
  return (
    <>
      <Container>
        <a href="/">
          <Title>MegaNets</Title>
        </a>

        <Content>
          <ul className="flex-container space-between">
            <li className="flex-item">
              <FontAwesomeIcon
                icon={faMapMarkerAlt}
                style={{ marginRight: "10px" }}
              />
              Cidade: São Paulo
            </li>
            <li className="flex-item">
              <FontAwesomeIcon
                icon={faPhoneAlt}
                style={{ marginRight: "10px" }}
              />
              Central de atendimento
            </li>
            <li className="flex-item">
              <a href="/wishlist">
                <FontAwesomeIcon
                  icon={faHeart}
                  style={{ marginRight: "10px" }}
                />
                Lista de desejo
              </a>
            </li>
          </ul>
          <label htmlFor="header-search" />
          <Input type="text" id="header-search" placeholder="Busca" name="s" />
        </Content>
      </Container>
    </>
  );
}
