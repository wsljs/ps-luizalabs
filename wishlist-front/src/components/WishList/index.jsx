import { useContext } from "react";
import ProductCard from "../ProductCard";
import { Container, Row, Column, Title } from "./style";
import { ProductsContext } from "../../ContextApi/ProductContext";
import { GetLocalStorage } from "../util.js";
import Loader from "react-loader-spinner";

export default function WishList() {
  const context = useContext(ProductsContext);
  const arrayWishlist = GetLocalStorage();

  return (
    <>
      <Container>
        <Title>Home - WishList</Title>
        {!context ? (
          <Loader
            type="Puff"
            color="#00BFFF"
            height={100}
            width={100}
            timeout={3000}
          />
        ) : (
          <Row className="Row">
            {context.products.map((product) => {
              if (arrayWishlist.indexOf(product.id) > -1) {
                return (
                  <Column key={product.id} mobile="6" tablet="12" desktop="12">
                    <ProductCard
                      key={product.id}
                      src={product.image}
                      title={product.title}
                      currencyFormat={product.currencyFormat}
                      price={product.price}
                      isWishlist={true}
                      id={product.id}
                    />
                  </Column>
                );
              }
            })}
          </Row>
        )}
      </Container>
    </>
  );
}
