import styled from "styled-components";

function getWidth(value) {
  if (!value) return;

  let width = (value / 12) * 100;
  return `width: ${width}`;
}

export const Container = styled.div`
  max-width: 1360px;
  padding-right: 15px;
  padding-left: 15px;
  margin: 0 auto;
  box-sizing: border-box;
  &:before,
  &:after {
    content: " ";
    display: table;
  }
  &:after {
    clear: both;
  }
`;

export const Row = styled.div`
  width: 100%;
  height: auto;
  float: left;
  box-sizing: border-box;
  &:before,
  &:after {
    content: " ";
    display: table;
  }
  &:after {
    clear: both;
  }
`;

export const Column = styled.div`
  float: left;
  padding: 0.25rem;
  min-height: 1px;
  box-sizing: border-box;

  @media only screen and (max-width: 768px) {
    ${({ mobile }) => mobile && getWidth(mobile)};
  }
  @media only screen and (min-width: 768px) {
    ${({ tablet }) => tablet && getWidth(tablet)};
  }
  @media only screen and (min-width: 1000px) {
    ${({ desktop }) => desktop && getWidth(desktop)};
  }
`;

export const Title = styled.h2`
  font-size: 3rem;
  text-align: left;
  margin: 15px;
  font-weight: 600;
`;
