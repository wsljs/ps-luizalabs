import { useContext, useState } from "react";
import ProductCard from "../ProductCard";
import { Container, Row, Column, Title } from "./style";
import { ProductsContext } from "../../ContextApi/ProductContext";

export default function Products() {
  const context = useContext(ProductsContext);

  return (
    <>
      <Container>
        <Title>Home</Title>
        {!context ? (
          <p> loading</p>
        ) : (
          <Row className="Row">
            {context.products.map((product) => (
              <Column key={product.id} mobile="6" tablet="12" desktop="12">
                <ProductCard
                  key={product.id}
                  src={product.image}
                  title={product.title}
                  currencyFormat={product.currencyFormat}
                  price={product.price}
                  id={product.id}
                  isWishlist={false}
                />
              </Column>
            ))}
          </Row>
        )}
      </Container>
    </>
  );
}
