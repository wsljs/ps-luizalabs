// import { useState } from "react";
import Header from "./components/Header";
import GlobalStyle from "./styles/global.js";
import { ProductsProvider } from "./ContextApi/ProductContext";
import Routes from "./components/Routes";

export function App() {
  return (
    <ProductsProvider>
      <Header />
      <Routes />
      <GlobalStyle />
    </ProductsProvider>
  );
}
