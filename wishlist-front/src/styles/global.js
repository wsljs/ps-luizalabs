import styled, { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
:root{
    --bg-color: #F9F9F9;
    --first-color: #4b0082;
}

 * {
     margin: 0;
     padding: 0;
     box-sizing: border-box
 }

 html{
     @media (max-width : 1080px){
         font-size: 93.75%;
     }
     
     @media (max-width: 720px){
         font-size: 87.50%;
     }
 }

 body{
     background: var(--bg-color);
     -webkit-font-smoothing: antialiased;
 }

 [disabled]{ 
     opacity: 0.7;
     cursor: not-allowed;
 }

 button {
     cursor: pointer;
 }
`;

export default GlobalStyle;
